import { UniqidTokenGenerator } from "../../../src/adapters/externalServices/UniqidTokenGenerator";
import uniqid from 'uniqid';

jest.mock("uniqid");
const mockUniqid = uniqid as jest.MockedFunction<typeof uniqid>;

describe('Uniqid Token Generator Tests', () => {
    it('should generate a token that is longer than 10 chars', () => {
        mockUniqid.mockReturnValue("1235qwerty")
        const uniqidTokenGenerator = new UniqidTokenGenerator();
        const token = uniqidTokenGenerator.generateToken();

        expect(token).toBe("1235qwerty");
        expect(token.length).toBeGreaterThanOrEqual(10);
    });

});
