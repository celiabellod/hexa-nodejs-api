import { UrlId } from "../../../src/domain/models/UrlId";
import { UrlIdTooShortError } from "../../../src/domain/models/errors/UrlIdTooShortError";

describe('UrlId Test', () => {
    it('should create an instance of Secret class', () => {
        expect(new UrlId("12356qwerty")).toBeInstanceOf(UrlId);
    });
    it('should throw an Error of the urlId has less than 10 chars', () => {
        expect(() => new UrlId("1235")).toThrow(UrlIdTooShortError);
    });
    it('should return a string representation on the toSring method', () => {
        expect(new UrlId("1235qwerty").toString()).toBe("1235qwerty");
    });
});
