import { Secret } from "../../../src/domain/models/Secret";
import { UrlId } from "../../../src/domain/models/UrlId";
import { SecretRepositoryInterface } from "../../../src/domain/ports/out/SecretRepositoryInterface";
import { SecretRetriever } from "../../../src/domain/useCases/SecretRetriever"

describe('Secret Retriever Test', () => {
    it('should retrieve a secret on time', async () => {
        const secretRepository: SecretRepositoryInterface = {
          getSecretByUrlId: jest.fn().mockResolvedValue(new Secret("123qwe")),
          removeSecretByUrlId: jest.fn(),
          storeUrlIdAndSecret: jest.fn()
        }
        const secretRetriever = new SecretRetriever(secretRepository);
        const urlId = new UrlId("1234qwerty");

        expect(await secretRetriever.retrieveSecret(urlId)).toEqual(new Secret("123qwe"));
        expect(secretRepository.getSecretByUrlId).toBeCalledTimes(1);
        expect(secretRepository.getSecretByUrlId).toBeCalledWith(urlId)
        expect(secretRepository.removeSecretByUrlId).toBeCalledTimes(1);
        expect(secretRepository.removeSecretByUrlId).toBeCalledWith(urlId)
    });
});
