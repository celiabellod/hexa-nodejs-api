import { Secret } from "../../../src/domain/models/Secret";
import { UrlId } from "../../../src/domain/models/UrlId";
import { SecretRepositoryInterface } from "../../../src/domain/ports/out/SecretRepositoryInterface";
import { SecretStorer } from "../../../src/domain/useCases/SecretStorer";
import { TokenGeneratorInterface } from "../../../src/domain/ports/out/TokenGeneratorInterface";


describe('Secret Storer Test', () => {
    it('should store a secret and return a urlId to query after', async () => {
      const secretRepository: SecretRepositoryInterface = {
        getSecretByUrlId: jest.fn().mockResolvedValue(new Secret("123qwe")),
        removeSecretByUrlId: jest.fn(),
        storeUrlIdAndSecret: jest.fn()
      }
      const tokenGenerator: TokenGeneratorInterface = {
        generateToken: jest.fn().mockReturnValue("123456qwerty"),
      }
      const secretStorer = new SecretStorer(secretRepository, tokenGenerator);
      const secret = new Secret("123qwe");
      const urlId = new UrlId("123456qwerty");

      expect(await secretStorer.storeSecret(secret)).toEqual(urlId);
      expect(secretRepository.storeUrlIdAndSecret).toBeCalledTimes(1);
      expect(secretRepository.storeUrlIdAndSecret).toBeCalledWith(urlId, secret)
      expect(tokenGenerator.generateToken).toBeCalledTimes(1);
    });
});
