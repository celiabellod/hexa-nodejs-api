import { Secret } from "../models/Secret";
import { UrlId } from "../models/UrlId";
import { SecretRepositoryInterface } from "../ports/out/SecretRepositoryInterface";
import { SecretRetrieverInterface } from "../ports/in/SecretRetrieverInterface";

export class SecretRetriever implements SecretRetrieverInterface {
  constructor(private secretRepository: SecretRepositoryInterface) {}

  async retrieveSecret(urlId: UrlId): Promise<Secret> {
    const secret = await this.secretRepository.getSecretByUrlId(urlId);
    await this.secretRepository.removeSecretByUrlId(urlId);
    return secret;
  }
}