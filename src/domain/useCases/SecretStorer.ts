import { Secret } from "../models/Secret";
import { UrlId } from "../models/UrlId";
import { SecretRepositoryInterface } from "../ports/out/SecretRepositoryInterface";
import { SecretStorerInterface } from "../ports/in/SecretStorerInterface";
import { TokenGeneratorInterface } from "../ports/out/TokenGeneratorInterface";

export class SecretStorer implements SecretStorerInterface {
  constructor(private secretRepository: SecretRepositoryInterface, private tokenGenerator: TokenGeneratorInterface) {}

  async storeSecret(secret: Secret): Promise<UrlId> {
    const token = this.tokenGenerator.generateToken();
    const urlId = new UrlId(token);
    await this.secretRepository.storeUrlIdAndSecret(urlId, secret);
    
    return urlId;
  }
  
}