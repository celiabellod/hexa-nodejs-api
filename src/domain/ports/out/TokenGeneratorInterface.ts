export interface TokenGeneratorInterface {
  generateToken(): string;
}