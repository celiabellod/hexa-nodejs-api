import { Secret } from "../../models/Secret";
import { UrlId } from "../../models/UrlId";

export interface SecretRepositoryInterface {
  getSecretByUrlId(urlId: UrlId): Promise<Secret>;
  removeSecretByUrlId(urlId: UrlId): Promise<void>;
  storeUrlIdAndSecret(urlId: UrlId, secret: Secret): Promise<void>;
}