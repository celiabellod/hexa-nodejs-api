import { Secret } from "../../models/Secret";
import { UrlId } from "../../models/UrlId";

export interface SecretStorerInterface {
  storeSecret(secret: Secret): Promise<UrlId>;
}