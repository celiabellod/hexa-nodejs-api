import { Secret } from "../../models/Secret";
import { UrlId } from "../../models/UrlId";

export interface SecretRetrieverInterface {
  retrieveSecret(urlId: UrlId): Promise<Secret>;
}