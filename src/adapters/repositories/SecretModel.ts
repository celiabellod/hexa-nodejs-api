import mongoose from "mongoose";
const { Schema } = mongoose;

interface ISecretSchema extends mongoose.Document {
  urlId: string,
  secret: string,
}

const SecretSchema = new Schema<ISecretSchema>({
  urlId: String,
  secret: String,
});

export const SecretModel = mongoose.model<ISecretSchema>('Secrets', SecretSchema)