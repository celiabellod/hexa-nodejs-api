import { TokenGeneratorInterface } from "../../domain/ports/out/TokenGeneratorInterface";
import uniqid from 'uniqid';

export class UniqidTokenGenerator implements TokenGeneratorInterface {
  generateToken(): string {
    return uniqid();
  }
}