import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { Secret } from "../../../domain/models/Secret";
import { SecretStorerInterface } from "../../../domain/ports/in/SecretStorerInterface";
import { ValidationError } from "./ValidationError";


export class SecretsController {
    constructor(private secretStorer: SecretStorerInterface) { }

    createSecret = async (request: Request, response: Response, next: NextFunction) => {
        try {
            this.validateRequest(request);

            const secret = new Secret(request.body.secret);
            const urlId = await this.secretStorer.storeSecret(secret);

            response.status(201).json(urlId);
        } catch (error) {
            next(error);
        }
    }

    private validateRequest(request: Request) {
        if (!request.body || !request.body?.secret || typeof request.body?.secret != "string")
            throw new ValidationError("Request body not valid");
    }
}