import { Request, Response, NextFunction } from "express";
import { UrlId } from "../../../domain/models/UrlId";
import { SecretRetrieverInterface } from "../../../domain/ports/in/SecretRetrieverInterface";
import { SecretRetriever } from "../../../domain/useCases/SecretRetriever";
import { ValidationError } from "./ValidationError";

export class SecretsByIdController {
    constructor(private secretRetriever: SecretRetrieverInterface) { }

    retrieveSecretByUrl = async (request: Request, response: Response, next: NextFunction) => {
        try {
            this.validateRequest(request);

            const urlId = new UrlId(request.params.urlId);
            const secret = await this.secretRetriever.retrieveSecret(urlId);

            response.status(200).json(secret);
        } catch (error) {
            next(error);
        }
    }

    private validateRequest(request: Request) {
        if (!request.params?.urlId)
            throw new ValidationError("URL is not valid");
    }
}